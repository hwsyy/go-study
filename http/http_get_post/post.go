package http_get_post

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

// 请求传递的参数，也就是 表单数据，保存在 *http.Request.Form 和 *http.Request.PostForm (POST 或 PUT 的数据)，
// 类似 PHP 的 $_REQUEST 和 $_POST/$_PUT 两个部分。

func MainPost() {

	//设置访问的路由
	http.HandleFunc("/", main_post)

	//设置监听的端口
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}

}

// 简单模板
const tpl = `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>表单</title>
	</head>
	<body>
		<form action="/?testid=123456&testname=laiki" method="post">
			name:<input type="text" name="name" />
			pwd :<input type="password" name="pwd" />
			<input type="submit"/>
		</form>
	</body>
</html>
`

func main_post(w http.ResponseWriter, r *http.Request) {

	// r.Method 获取请求的方法
	// 判断是否为 GET POST
	if r.Method == "GET" {

		// 创建模板对象(给模版起个名字)，并引入模板
		t,_ := template.New("Posttpl").Parse(tpl)
		// 输出流数据
		t.Execute(w, nil)

	} else {

		/*
			r.Form里面包含了所有请求的参数，比如URL中query-string、
			POST的数据、PUT的数据，所有当你在URL的query-string字段和POST冲突时，
			会保存成一个slice，里面存储了多个值，Go官方文档中说在接下来的版本里面
			将会把POST、GET这些数据分离开来。

			Request本身也提供了FormValue()函数来获取用户提交的参数。
			如r.Form["username"]也可写成r.FormValue("username")。
			调用r.FormValue时会自动调用r.ParseForm，所以不必提前调用。
			r.FormValue只会返回同名参数中的第一个，若参数不存在则返回空字符串。
		*/

		/**
		 * 第一种
		 */

		// 解释表单数据，默认是不会解析的，否则 r.Form 是空的
		//r.ParseForm()
		// 打印表单数据
		//fmt.Fprintf(w, "%v",r.Form)

		/**
		 * 第二种
		 */

		// 让Go自动解释,调用 r.FormValue() 的时候会自动执行 r.ParseForm()
		fmt.Fprintf(w, "name=%s \r\n",r.FormValue("name"))
		fmt.Fprintf(w, "pwd=%s \r\n",r.FormValue("pwd"))
	}

}
