package http_get_post

import (
	"fmt"
	"log"
	"net/http"
)

// HTTP 过程的操作主要是针对客户端发来的请求数据在 *http.Request，和返回给客户端的 http.ResponseWriter 两部分。
// 请求数据 *http.Request 有两个部分：基本数据和传递参数。基本数据比如请求的方法、协议、URL、头信息等可以直接简单获取

func MainGet() {

	http.HandleFunc("/", main_get)         //设置访问的路由
	err := http.ListenAndServe(":80", nil) //设置监听的端口
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func main_get(w http.ResponseWriter, r *http.Request) {

	// 解析参数，默认是不会解析的
	r.ParseForm()

	// 第一种
	// r.Form 存放 所有 get 请求参数
	fmt.Fprintf(w, "%v", r.Form)

	// 第二种
	// 让Go自动解释,调用 r.FormValue() 的时候会自动执行 r.ParseForm()
	// 单个获取
	//fmt.Fprintf(w, r.FormValue("name"))

	// 这个写入到 w 的是输出到客户端的
	fmt.Fprintf(w, "Hello go get!")
}
