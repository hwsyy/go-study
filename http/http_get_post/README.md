
#### Request： 用户请求的信息，用来解析用户的请求信息，包括 post、get、cookie、url 等信息
#### Response：服务器需要反馈给客户端的信息
#### Conn：    用户的每次请求链接
#### Handler： 处理请求和生成返回信息的处理逻辑

#### Form：    存储了 post、put和get 参数，在使用之前需要调用 ParseForm 方法。
#### PostForm：存储了 post、put      参数，在使用之前需要调用 ParseForm 方法。
#### MultipartForm：存储了包含了文件上传的表单的 post 参数，在使用前需要调用 ParseMultipartForm 方法。

#### POST、GET 应用
#### r.Form 里面包含了所有请求的参数，比如URL中 query-string、 POST 的数据、PUT 的数据，
#### 所有当你在 URL 的 query-string 字段和POST冲突时， 会保存成一个 slice，
#### 里面存储了多个值，Go 官方文档中说在接下来的版本里面 将会把 POST、GET 这些数据分离开来。

#### Request 本身也提供了 FormValue() 函数来获取用户提交的参数。
#### 如 r.Form["username"] 也可写成 r.FormValue("username") 。
#### 调用 r.FormValue 时会自动调用 r.ParseForm ，所以不必提前调用。
#### r.FormValue 只会返回同名参数中的第一个，若参数不存在则返回空字符串。

#### 第一种，解释表单数据，默认是不会解析的，否则 r.Form 是空的，手动调用 r.ParseForm()
#### 第二种，让Go自动解释,调用 r.FormValue() 的时候会自动执行 r.ParseForm()