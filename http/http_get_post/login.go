package http_get_post

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func MainLogin() {

	http.HandleFunc("/", main_login)         // 设置访问的路由 - 加载文件模板
	http.HandleFunc("/func", main_loginFunc) // 设置访问的路由 - 加载文件模板，并注册模板函数

	err := http.ListenAndServe(":80", nil) // 设置监听的端口
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}

}

// 加载文件模板
func main_login(w http.ResponseWriter, r *http.Request) {

	// r.Method 获取请求的方法
	// 判断是否为 GET POST
	if r.Method == "GET" {

		// 加载模板文件
		t, _ := template.ParseFiles("./http/http_get_post/login.html")
		// 输出流数据
		t.Execute(w, nil)

	} else {

		// 让Go自动解释,调用 r.FormValue() 的时候会自动执行 r.ParseForm()
		fmt.Fprintf(w, "username=%s \r\n", r.FormValue("username"))
		fmt.Fprintf(w, "password=%s \r\n", r.FormValue("password"))

	}

}

// 加载文件模板，并注册模板函数
func main_loginFunc(w http.ResponseWriter, r *http.Request) {

	// r.Method 获取请求的方法
	// 判断是否为 GET POST
	if r.Method == "GET" {

		// 注册模板函数
		tempfunc := make(template.FuncMap)
		tempfunc["TpmFuncId"] = TpmFuncId
		tempfunc["TpmFuncIds"] = TpmFuncIds

		// 要向给模板注册函数，一定要给模版起个名字,进行注册模板函数,加载模板文件
		t, _ := template.New("loginfunc.html").Funcs(tempfunc).ParseFiles("./http/http_get_post/loginfunc.html")

		// 输出流数据
		t.Execute(w, map[string]interface{}{"name": "laixhe", "age": 18})

	} else {

		// 让Go自动解释,调用 r.FormValue() 的时候会自动执行 r.ParseForm()
		fmt.Fprintf(w, "username 1=%s \r\n", r.FormValue("username"))
		fmt.Fprintf(w, "password 1=%s \r\n", r.FormValue("password"))

	}

}

func TpmFuncId(id int) int {
	return id * id
}

func TpmFuncIds(id, ids int) int {
	return id * ids
}
