package cross_domain

import (
	"log"
	"net/http"
)

/**
 * Go 原生支持跨域请求的方法
 * 一般浏览器在跨域请求前会发个 options 请求来验证是否跨域，
 * 所以后端再处理这个 options 请求时，要告诉浏览器一些信息，
 * 其实就是个 header 信息。
 */

func CrossDomain() {

	//设置访问的路由
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		//允许跨域(允许任何访问、包括ajax跨域)
		w.Header().Add("Access-Control-Allow-Origin", "*")
		//w.Header().Add("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE")
		//w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		//w.Header().Add("Access-Control-Allow-Credentials", "true")

		// options 请求就只需要输出头部信息就行
		if r.Method == "OPTIONS" {
			//返回状态码200
			w.WriteHeader(http.StatusOK)
			return
		}

		//这个写入到 w 的是输出到客户端的
		w.Write([]byte("123"))

	})

	//设置监听的端口
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal("监听端口失败: ", err)
	}
}
