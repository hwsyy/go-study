package file_server

import (
	"log"
	"net/http"
	"os"
)

/**
 * Go 原生支持 静态服务器
 */

// http.Dir() 该路径指向的是系统里的绝对路径
// 将 http.Dir() 的路径改为运行时的相对路径,使用 http.StripPrefix() 方法
// http.StripPrefix("/static/", http.FileServer(http.Dir("./static")))

func FileServer() {

	//设置访问的路由
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		//获取当前路径
		wd, err := os.Getwd()
		if err != nil {
			log.Fatal(err)
		}

		http.FileServer(http.Dir(wd)).ServeHTTP(w, r)
	})

	//设置监听的端口
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal("监听端口失败: ", err)
	}
}

func handle() {

	//设置访问的路由
	http.Handle("/", http.FileServer(http.Dir("./")))

	//设置监听的端口
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal("监听端口失败: ", err)
	}
}
