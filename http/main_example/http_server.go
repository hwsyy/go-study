package main_example

import (
	"log"
	"net/http"
	"time"
)

// 最后 配置 http.Server 服务
func MainHttpServer() {

	serveMux := http.NewServeMux()
	serveMux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello world, this is version 4."))
	})

	//手工配置 http.Server 服务
	server := http.Server{
		Addr:         ":80",         // 监听地址和端口
		Handler:      serveMux,        // Handle
		ReadTimeout:  5 * time.Second, // 读超时
		WriteTimeout: 5 * time.Second, // 写超时
	}

	// 启动监听
	err := server.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
