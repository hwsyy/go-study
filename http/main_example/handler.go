package main_example

import (
	"fmt"
	"net/http"
)

/*

Go 默认的路由匹配机制很弱。mainHandleFunc 的代码除了 /abc，其他的请求都匹配到 / ，不足以使用，
肯定需要自己写路由的过程。一个简单的方式就是写一个自己的 http.Handler

*/

// 实现 http.Handler 接口的 ServeHTTP 方法
type MyHandler struct{}
// 必须实现 Handler 的 ServeHTTP 方法(继承)
func (mh MyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// 配置相应路由规则
	if r.URL.Path == "/abc" {
		w.Write([]byte("abc"))
		return
	}

	if r.URL.Path == "/xyz" {
		w.Write([]byte("xyz"))
		return
	}

	w.Write([]byte("Hello world, this is version 2."))

	// 这里你可以写自己的路由匹配规则

}

// 简单的实现了自己的 http Handle
func MainHandler() {

	//两种方式

	/*
		http.Handle("/", MyHandler{})

		if err := http.ListenAndServe(":8080", nil); err != nil {
			fmt.Println("start http server fail:", err)
		}
	*/

	// 设置监听的端口
	if err := http.ListenAndServe(":80", MyHandler{}); err != nil {
		fmt.Println("start http server fail:", err)
	}
}
