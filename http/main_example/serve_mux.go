package main_example

import (
	"io"
	"log"
	"net/http"
)

/*

net/http提供了一个非常简单的路由结构 http.ServeMux。方法 http.HandleFunc()和
        http.Handler() 就是把路由规则和对应函数注册到默认的一个 http.ServeMux 上。
		当然，你可以自己创建 http.ServeMux 来使用.


但是因为 http.ServeMux 路由规则简单，功能有限。
更推荐使用 httprouter、mux 等第三方
*/

func MainServeMux() {

	mux := http.NewServeMux()

	// 设置访问的路由
	mux.Handle("/", &myHandler{})
	mux.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hello world, this is version 3.")
	})

	// 设置监听的端口
	err := http.ListenAndServe(":80", mux)
	if err != nil {
		log.Fatal(err)
	}

}

// 定义一个结构(用于实现自己的Handler)
type myHandler struct{}

// 必须实现 Handler 的 ServeHTTP 方法(继承)
func (*myHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "URL2:"+r.URL.String())
}
