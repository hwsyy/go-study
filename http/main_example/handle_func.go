package main_example

import (
	"io"
	"log"
	"net/http"
)

// 简单的 http
func MainHandleFunc() {

	// 设置访问的路由-首页
	http.HandleFunc("/", sayHello)

	// 路由-abc
	http.HandleFunc("/abc", func(w http.ResponseWriter, r *http.Request) {
		//向浏览器输出内容
		w.Write([]byte("Hello, Go HTTP abc"))
	})

	// 启动web服务，监听 80 端口
	err := http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}

}

// w表示response对象，返回给客户端的内容都在对象里处理
// r表示客户端请求对象，包含了请求头，请求参数等等
func sayHello(w http.ResponseWriter, r *http.Request) {

	// 向浏览器输出内容 - 方法 1
	//w.Write([]byte("Hello world, this is version 1."))

	// 向浏览器输出内容 - 方法 2
	io.WriteString(w, "Hello world, this is version 1.")

	// 向浏览器输出内容 - 方法 3
	//fmt.Fprintf(w, "Hello world, this is version 1.")

}

/*

Go 默认的路由匹配机制很弱。上面的代码除了 /abc，其他的请求都匹配到 / ，不足以使用，
肯定需要自己写路由的过程。一个简单的方式就是写一个自己的 http.Handler

*/

