#### Request：用户请求的信息，用来解析用户的请求信息，包括post、get、cookie、url等信息

#### Response：服务器需要反馈给客户端的信息

#### Conn：用户的每次请求链接

#### Handler：处理请求和生成返回信息的处理逻辑

##### 简单的 http
MainHandleFunc()

##### 简单的实现了自己的 http Handle
MainHandler()

##### 使用 http.ServeMux 路由规则简单
MainServeMux()

##### 最后 配置 http.Server 服务
MainHttpServer()