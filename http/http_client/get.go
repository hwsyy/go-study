package http_client

import (
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
	"fmt"
)


//获取课程网页
func HttpGetSimple(url string) {

	//获取当前时间戳
	//getStart := time.Now().Unix()
	//获取当前时间戳,精确到纳秒
	getStart := time.Now().UnixNano()
	//将纳秒转换为毫秒
	getStart = getStart / 1e6

	//远程获取网页 - GET方式
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("resp:", err)
		return
	}
	defer resp.Body.Close()

	//获取网页的所有内容
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("body:", err)
		return
	}

	//获取当前时间戳,将纳秒转换为毫秒   1e6 就是 10^6 (10的6次方)
	getEnd := time.Now().UnixNano() / 1e6
	//执行时间，并转为 浮点型
	endTime := float64(getEnd-getStart) / 1e3

	fmt.Println(" 请求状态：" + strconv.Itoa(resp.StatusCode) + " 请求耗时：" + strconv.FormatFloat(endTime, 'f', 3, 64) + "s")

	fmt.Println(string(body))

}
