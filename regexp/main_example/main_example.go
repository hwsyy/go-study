package main_example

import (
	"log"
	"regexp"
)

func Example() {

	text := `regexp Hello regexp`

	// Hello 替换 Go
	reg := regexp.MustCompile(`Hello`)
	log.Println("替换: ", reg.ReplaceAllString(text, "Go"))

	// 中英文之间加空格
	text = "中a英bc文之1间加2,3空格"
	g1 := regexp.MustCompile("([\u4e00-\u9fa5])(\\w)")
	g2 := regexp.MustCompile("(\\w)([\u4e00-\u9fa5])")
	g3 := g2.ReplaceAllString(g1.ReplaceAllString(text, "$1 $2"), "$1 $2")
	log.Println("空格: ", g3)

	// 匹配正则表达式
	regstr := "123456"
	log.Println(regexp.MatchString(`^[0-9]+$`, regstr))

	// 获取匹配的内容
	str := "abc azc a7c aac 888 a9c taa1c"
	reg1 := regexp.MustCompile(`a\dc`)
	log.Println("获取匹配的内容: ", reg1.FindAllString(str, -1))

}
