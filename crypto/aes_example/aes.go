package aes_example

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
)

// 秘钥长度需要时AES-128(16bytes)或者AES-256(32bytes)
func main() {

	orig := "hello world 对称加密算法,也就是加密和解密用相同的密钥"

	key := "1234567812345623fqwefwefweq34fwe"
	fmt.Println("原文：", orig)

	encryptCode := AesEncrypt(orig, key)
	fmt.Println("密文：", encryptCode)

	decryptCode := AesDecrypt(encryptCode, key)
	fmt.Println("解密结果：", decryptCode)
}

// 使用 AES 加密文本,加密的文本不能为空
func AesEncrypt(orig string, key string) string {

	// 转成字节数组
	origData := []byte(orig)
	k := []byte(key)

	// 分组秘钥
	block, _ := aes.NewCipher(k)
	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 补全码
	origData = PKCS7Padding(origData, blockSize)
	// 加密模式
	blockMode := cipher.NewCBCEncrypter(block, k[:blockSize])
	// 创建数组
	cryted := make([]byte, len(origData))
	// 加密
	blockMode.CryptBlocks(cryted, origData)

	return base64.StdEncoding.EncodeToString(cryted)

}

// 使用 AES 解密文本
func AesDecrypt(cryted string, key string) string {

	// 转成字节数组
	crytedByte, _ := base64.StdEncoding.DecodeString(cryted)
	k := []byte(key)

	// 分组秘钥
	block, _ := aes.NewCipher(k)
	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 加密模式
	blockMode := cipher.NewCBCDecrypter(block, k[:blockSize])
	// 创建数组
	orig := make([]byte, len(crytedByte))
	// 解密
	blockMode.CryptBlocks(orig, crytedByte)
	// 去补全码
	orig = PKCS7UnPadding(orig)
	return string(orig)
}

// 使用 AES 加密文本的时候文本必须定长,即必须是 16,24,32 的整数倍
func PKCS7Padding(ciphertext []byte, blocksize int) []byte {

	padding := blocksize - len(ciphertext)%blocksize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

// 使用 AES 解密文本,解密收删除 padding 的文本
func PKCS7UnPadding(origData []byte) []byte {

	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
