package aes_example

import (
	"testing"
	"fmt"
)

const KEY  = "1234567812345623fqwefwefweq34fwe"

const OEIG  = "hello world 对称加密算法,也就是加密和解密用相同的密钥"

func TestAesDecrypt(t *testing.T) {

	encryptCode := AesEncrypt(OEIG, KEY)
	fmt.Println("加密：", encryptCode)

}

func TestAesEncrypt(t *testing.T) {

	// 加密
	encryptCode := "0b3/rJYsqDgJVGNBKZEc13o0z6rksmN0rZX3LKhQ/HqFEuO3q7d5TVjA2Ka4XmabAIqgaMZKItUabDXsSjhZhfa3SNbnGtZ3Io4VPngeXPw="

	decryptCode := AesDecrypt(encryptCode, KEY)
	fmt.Println("解密：", decryptCode)
}
