package md5_example

import (
	"crypto/md5"
	"fmt"
)

// 从计算效果看： md5.Sum(bdata) = hash.Write(data) + hash.Sum(nil)

// 123456 结果都一样：   e10adc3949ba59abbe56e057f20f883e

func Md5(encrypt string) string {

	// 等待要加密的数据
	data := []byte(encrypt)

	// 初始化 MD5 实例
	hash := md5.New()

	// 写入 MD5 的缓存，等待计算
	hash.Write(data)

	// 进行 MD5 算计，返回 16进制的 byte 数组
	sumData := hash.Sum(nil)

	return fmt.Sprintf("%x\n", sumData)
}

func Md5Sun(encrypt string) string {

	// 等待要加密的数据
	data := []byte(encrypt)

	// 直接进行 MD5 算计，返回 16进制的 byte 数组
	sumData := md5.Sum(data)

	return fmt.Sprintf("%x\n", sumData)

}
