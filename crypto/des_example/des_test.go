package des_example

import (
	"testing"
	"fmt"
)

const OEIG = "Hello World! 对称加密算法,也就是加密和解密用相同的密钥"

// DES 声明秘钥,利用此秘钥实现明文的加密和密文的解密，长度必须为8
const DESkey = "123ABs78"

// 3DES 的秘钥长度必须为24位
const DESkey3 = "123456781234567812345678"

// DES 加密
func TestMyDesEncrypt(t *testing.T) {

	encyptCode := MyDesEncrypt(OEIG, DESkey)
	fmt.Println("DES 密文：", encyptCode)

}

// DES 解密
func TestMyDESDecrypt(t *testing.T) {

	// 加密
	encyptCode := "TQ+rQfg9eatxUVdyCIGoXk7edrgDUOAre9yN8psVskzwM5tHyzq/+62acTiWNLcIwFQXLpNpyX8rrUWkzE9PYou9EKOYRa7202d1KjeidDs="

	// DES 解密
	decyptCode := MyDESDecrypt(encyptCode, DESkey)
	fmt.Println("DES 解密结果：", decyptCode)

}


// 3DES 加密
func TestTripleDesEncrypt(t *testing.T) {

	encryptCode := TripleDesEncrypt(OEIG, DESkey3)
	fmt.Println("3DES 密文：", encryptCode)

}


// DES 加密方法
func TestTipleDesDecrypt(t *testing.T) {

	encryptCode := "w3VRu3f3QdBO0yuTITEGdCRFMZtv3ZeAtMbbxiRGzfGlKL4HIWtpY8uWdk8tFDysl2W0et1kiiECRhPnd/gl9fNynQwI0p6skQJbD5wdlKA="

	// 3DES 加密
	decryptCode := TipleDesDecrypt(encryptCode, DESkey3)
	fmt.Println("3DES 解密结果：", decryptCode)
}
