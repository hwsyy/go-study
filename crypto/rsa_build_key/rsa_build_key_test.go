package rsa_build_key

import (
	"testing"
	"log"
)

func TestGenRsaKey(t *testing.T) {

	// 密钥长度
	if err := GenRsaKey(1024); err != nil {
		log.Fatal("密钥文件生成失败！")
	}
	log.Println("密钥文件生成成功！")

}
