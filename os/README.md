
### 操作系统

####  args             命令行参数处理
####  file_example     文件操作
####  dir              目录操作
####  file_dir_info    文件或文件夹的信息
####  simple           简单的例子

## 终端的输入输出

#### os.Stdin   标准输入
#### os.Stdout  标准输出
#### os.Stderr  标准错误输出