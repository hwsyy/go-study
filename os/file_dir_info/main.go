package main

import (
	"fmt"
	"log"
	"os"
)

func main() {

	// 获取文件或文件夹的信息
	info, err := os.Stat("./file_dir_info.exe")
	if err != nil {
		// 如果返回的错误类型使用 os.IsNotExist() 判断为 true,说明文件或文件夹不存在
		return
	}

	log.Println(os.IsNotExist(err))
	log.Println(os.IsExist(err))

	fmt.Println(info.Name())    // 获取文件名
	fmt.Println(info.IsDir())   // 判断是否是目录，返回bool类型
	fmt.Println(info.ModTime()) // 获取文件修改时间
	fmt.Println(info.Mode())    // 文件的权限
	fmt.Println(info.Size())    // 获取文件大小

	// ==============
	dir, _ := os.Getwd()
	fmt.Println("当前的目录:", dir)

	// 切换目录
	//os.Chdir("../")

	// 主机名
	//osName, _ := os.Hostname()
	//fmt.Println("主机名:", osName)

	// 输出所有环境变量
	//path := os.Environ()
	//fmt.Println("输出所有环境变量:", path)
	// 清空环境变量
	//os.Clearenv()

	//GOPATH := os.Getenv("GOPATH")
	//fmt.Println("环境变量GOPATH的值是:", GOPATH)

	// 改变的是文件的权限
	//os.Chmod("./file_dir_info.exe", 0777)
	// 改变文件的时间-访问时间 创建时间
	//os.Chtimes("./file_dir_info.exe", time.Now(), time.Now())
	// 重命名
	//os.Rename("1.go", "2.go")
}
