package args

import (
	"fmt"
	"os"
)

func Args() {

	fmt.Println("args[0]=", os.Args[0])

	if len(os.Args) > 1 {

		for key, value := range os.Args {

			if key == 0 {
				continue
			}

			fmt.Printf("args[%d]=%v\n", key, value)

		}

	}

}
