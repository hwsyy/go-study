package simple

import (
	"os/exec"
	"os"
	"path/filepath"
)

// 获取可执行文件的绝对路径(不包括文件名)
func GetCurrPath() string {

	// 从环境变量中搜索可执行的二进制的文件的路径，
	// 如果 file 中包含一个斜杠，
	// 则直接根据绝对路径或者相对本目录的相对路径去查找
	file, _ := exec.LookPath(os.Args[0])

	// 检测地址是否是绝对地址，是绝对地址直接返回，
	// 不是绝对地址，会添加当前工作路径
	path, _ := filepath.Abs(file)
	return filepath.Dir(path)
}
