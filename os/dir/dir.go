package dir

import "os"

// 目录操作

// 添加目录
func MkDir(){

	// 创建名称为 test 的目录，设置权限为 0777，相当于 Linux 系统中的          mkdir test
	os.Mkdir("./test", 0777)
	// 创建 test_dir/code/go 多级子目录，设置权限为 0777，相当于 Linux 中的   mkdir -p test_dir/code/go
	os.MkdirAll("./test_dir/code/go", 0777)

}

// 删除目录
func DelDir(){

	// 删除目录，当目录下有文件或者其他目录是会出错
	os.Remove("./test")
	// 根据path删除多级子目录，如果 path是单个名称，那么该目录不删除
	os.RemoveAll("./test_dir")
}