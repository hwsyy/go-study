package main_example

import (
	"bytes"
	"log"
	"os/exec"
	"strings"
	"time"
)

// 定时器，每隔2秒运行一次
func TimeTick() {

	ticker := time.Tick(time.Second * 2)

	for {
		select {
		case <-ticker:

			go Netstat()
		}
	}
}

func Netstat() {

	cmd := exec.Command("netstat", "-ntlp")

	var out bytes.Buffer
	cmd.Stdout = &out

	// 运行命令
	if err := cmd.Run(); err != nil {
		log.Println("cmd.Run", err)
		return
	}

	str := out.String()

	if !strings.Contains(str, "zimg") {

		zimg := exec.Command("/usr/local/zimg/zimg", "-d", "/usr/local/zimg/conf/zimg.lua")
		_, err := zimg.Output()
		if err != nil {
			log.Println("zimg.Output", err)
			return
		}

	}

}
