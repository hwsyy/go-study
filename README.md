
# 主要记录平时学习Go语言

### context                     学习 Context 在Go1.7引入了

### crypto                      常用的密码（算法）使用

### fmt                         格式化输入输出基本使用

### flag                        命令行参数处理

### http                        学习 net/http

### os                          操作系统(文件|目录)

### reflect                     反射基本操作

### regexp                      常用正则操作

### socket                      学习 TCP UDP

### sort                        常用的基本算法

### sync                        同步和锁

### library                     学习第三方库

### strconv                     数据类型转换

### strings                     常用字符串操作

### time                        常用时间操作

### blockchain                  区块链
