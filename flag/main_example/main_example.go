package main_example

import (
	"flag"
	"fmt"
)

// 将命令行的参数解析到全局变量
var (
	mode = flag.String("mode", "", "运行模式")
	port = flag.Int("port", 5050, "服务端监听端口")
	file = flag.String("file", "", "文件地址名称")
)

func Flag(){

	// 解析命令行的参数
	flag.Parse()
	// 打印命令行接收到的参数数据
	fmt.Println(*mode, *port, *file)

}
