
### 常用时间操作

#### 获取当前时间,返回时间对象
now := time.Now()

#### 年
year := now.Year()

#### 月
mont := now.Month()

#### 日
day := now.Day()

#### 时
hour := now.Hour()

#### 分
minute := now.Minute()

#### 秒
send := now.Second()
fmt.Printf("%02d-%02d-%02d %02d:%02d:%02d \n", year, mont, day, hour, minute, send)

#### 比较两个time 的大小
t2 := time.Now()

#### 两个时间是否相等
fmt.Println(now.Equal(t2))

#### 相差的纳秒数
fmt.Println(t2.Sub(now))

#### 获取当前时间戳
timStamp := time.Now().Unix() ####.UnixNano()包含纳秒数
fmt.Println(timStamp)

#### 将时间戳转为时间对象
now = time.Unix(timStamp, 0)

#### 时间格式化 - Go语言的时间格式化比较奇葩 2006年01月02日15时04分05秒 是固写法
timeFt := now.Format("2006-01-02 15:04:05")
fmt.Println(timeFt)

#### 睡眠 1秒
time.Sleep(time.Second * 1)

#### 计算运行的时间
fmt.Println(time.Since(now))

#### 时间字符串转换时间戳 - 先用time.Parse对时间字符串进行分析，如果正确会得到一个time.Time对象
time.Parse("2006-01-02 15:04:05", "2018-01-06 16:12:00")
