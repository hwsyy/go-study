package main

import(
	"net"
	"log"
)

func main() {
	ListenPacket()
}

func ListenPacket() {

	// PacketConn 不支持 io.reader 和 io.writer
	// 但是他支持两个替代方法 ReadFrom() 和 WriteTo()

	udp, err := net.ListenPacket("udp", ":8015")
	if err != nil {
		log.Fatal(err)
	}
	defer udp.Close()

	log.Printf("Local: <%s> \n", udp.LocalAddr().String())

	data := make([]byte, 1024)
	for {

		n, addr, err := udp.ReadFrom(data)
		if err != nil {
			log.Printf("error during read: %s", err)
		}

		log.Printf("<%s> %s\n", addr, data[:n])

		udp.WriteTo([]byte("is server Packet"), addr)

	}

}

// 通过 Dial 方式发送数据报的例子
func ListenUDP(){

	addr := net.UDPAddr{IP:net.ParseIP("0.0.0.0"),Port:8015}
	udp, err := net.ListenUDP("udp", &addr)
	if err != nil {
		log.Println(err)
		return
	}
	defer udp.Close()

	log.Printf("Local: <%s> \n", udp.LocalAddr().String())

	data := make([]byte, 1024)
	for {
		n, reAddr, err := udp.ReadFromUDP(data)
		if err != nil {
			log.Printf("error during read: %s", err)
		}

		log.Printf("<%s> %s\n", reAddr, data[:n])

		udp.WriteToUDP([]byte("is server ListenUDP"), reAddr)
	}


}