package main

import(
	"net"
	"log"
)

func main() {
	DialUDP()
}

//
func DialUDP() {

	// 客户端
	srcAddr := &net.UDPAddr{IP: net.IPv4zero, Port: 8016} // Port 为 0 为随机端口
	// 服务端
	dstAddr := &net.UDPAddr{IP: net.ParseIP("192.168.1.20"), Port: 8015}

	udp, err := net.DialUDP("udp", srcAddr, dstAddr)
	if err != nil {
		log.Println(err)
		return
	}
	defer udp.Close()

	log.Printf("Local: <%s> \n", udp.LocalAddr().String())

	_, err = udp.Write([]byte("is DialUDP"))
	if err != nil {
		log.Println(err)
		return
	}

	data := make([]byte, 1024)

	n, err := udp.Read(data)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println(string(data[:n]))

}

// 基于 Dial 实现客户端，TCP和UDP的实现方式没有什么明显的区别
func Dial(){

	udp, err := net.Dial("udp", "192.168.1.20:8015")
	if err != nil {
		log.Println(err)
		return
	}
	defer udp.Close()

	log.Printf("Local: <%s> \n", udp.LocalAddr().String())

	_, err = udp.Write([]byte("is Dial"))
	if err != nil {
		log.Println(err)
		return
	}

	data := make([]byte, 1024)

	n, err := udp.Read(data)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println(string(data[:n]))

}
