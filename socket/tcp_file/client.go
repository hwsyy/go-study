package main

import (
	"io"
	"log"
	"net"
	"os"
	"strconv" //基本类型转换
)

/**
 * 客户端
 * @param int port 端口号
 * @param string file 文件地址名称
 */
func runClient(port int, file string) {

	// 连接服务端
	conn, err := net.Dial("tcp", "127.0.0.1:"+strconv.Itoa(port))
	if err != nil {
		log.Printf("无法建立链接：%v", err)
		return
	}
	defer conn.Close() //关闭链接
	log.Println("链接建立成功！")

	//*****************1、打开文件流和发送头信息(文件名)**********************

	// 打开要传送的文件,返回文件流
	f, err := os.Open(file)
	if err != nil {
		log.Printf("无法打开文件：%v", err)
		return
	}
	defer f.Close() //关闭打开的文件

	// 写入头信息并等待确认
	conn.Write([]byte(file)) //发送网络流信息

	//*****************2、接收服务端的回复信息**********************

	// 接收服务端的回复信息
	p := make([]byte, 2)
	// 读取网络流的字节
	_, err = conn.Read(p)
	if err != nil {
		log.Printf("无法获得服务端信息：%v", err)
		return
	} else if string(p) != "ok" {
		log.Printf("无效的服务端响应：%s", string(p))
		return
	}

	log.Println("接收服务端的回复信息(文件名)成功！")

	//*****************3、从复制本地文件流到网络流上**********************

	// 复制本地文件流到网络流
	_, err = io.Copy(conn, f)
	if err != nil {
		log.Printf("发送文件失败(%s):%v", file, err)
		return
	}
	log.Printf("文件发送成功(%s)", file)
}
