package main

import (
	"flag" //处理命令行
	"fmt"  //处理输入输出
	"log"  //处理日志
)

// Go语言TCP文件传输

// 将命令行的参数解析到全局变量
var (
	mode = flag.String("mode", "", "运行模式")
	port = flag.Int("port", 5050, "服务端监听端口")
	file = flag.String("file", "", "文件地址名称")
)

func main() {

	// 解析命令行的参数
	flag.Parse()
	// 打印命令行接收到的参数数据
	fmt.Println(*mode, *port, *file)

	// 判断运行模式
	switch *mode {
	case "server":
		// 服务端
		runServer(*port)

	case "client":
		// 客户端
		runClient(*port, *file)

	default:
		log.Fatalf("未知运行模式：%v", *mode)
	}
}
