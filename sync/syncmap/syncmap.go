package syncmap

import (
	"sync"
)

//RWMutex提供了四个方法：
//func (*RWMutex) Lock    // 写锁定
//func (*RWMutex) Unlock  // 写解锁
//func (*RWMutex) RLock   // 读锁定
//func (*RWMutex) RUnlock // 读解锁

//安全的Map
type syncMap struct {
	rw   *sync.RWMutex
	data map[interface{}]interface{}
}

//存储操作
func (sm *syncMap) Put(k, v interface{}) {
	sm.rw.Lock()
	defer sm.rw.Unlock()
	sm.data[k] = v
}

//获取操作
func (sm *syncMap) Get(k interface{}) interface{} {
	sm.rw.RLock()
	defer sm.rw.RUnlock()
	return sm.data[k]
}

//删除操作
func (sm *syncMap) Delete(k interface{}) {
	sm.rw.Lock()
	defer sm.rw.Unlock()
	delete(sm.data, k)
}

//遍历Map，并且把遍历的值给回调函数，可以让调用者控制做任何事情
func (sm *syncMap) Each(cb func(interface{}, interface{})) {
	sm.rw.RLock()
	defer sm.rw.RUnlock()
	for k, v := range sm.data {
		cb(k, v)
	}
}

//生成初始化一个 syncMap
func NewSyncMap() *syncMap {
	return &syncMap{
		rw:   new(sync.RWMutex),
		data: make(map[interface{}]interface{}),
	}
}
