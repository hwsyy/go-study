package main_example

import "fmt"

// 格式化输入，空格作为分隔符，占位符和格式化输出一致
func TestScanf(){
	var a int
	var b string
	var c float32

	fmt.Scanf("%d%s%f", &a, &b, &c)
	fmt.Println(a, b, c)
}

// 从终端获取用户输入，存储在 Scanln 中的参数里，空格和换行符，作业分隔符
func TestScan(){
	var a int
	var b string
	var c float32

	fmt.Scan(&a, &b, &c)
	fmt.Println(a, b, c)
}

// 从终端获取用户输入，存储在 Scanln 中的参数里，空格作为分隔符，遇到换行符结束
func TestScanln(){
	var a int
	var b string
	var c float32

	fmt.Scanln(&a, &b, &c)
	fmt.Println(a, b, c)
}
