
## 常用字符串操作

#### strings.HasPrefix(s, prefix string) bool            判断字符串 s 是否以 prefix 开头
#### strings.HasSuffix(s, suffix string) bool            判断字符串 s 是否以 suffix 结尾

#### strings.Contains(s, substr string) bool             判断字符串 s 是否包含 substr

#### strings.Index(s, str string) int                    判断子字符串在父字符串中第一次出现的位置
####                                                         (-1表示字符串 str 不包含字符串 substr )，索引从0开始
#### strings.LastIndex(s, str string) int                判断子字符串在父字符串中最后出现的位置
####                                                         (-1表示字符串 str 不包含字符串 substr )，索引从0开始

#### strings.IndexRune(s, r rune) int                    判断 ASCII 编码的字符在父字符串中最后出现的位置

#### strings.Count(s, str string) int                    计算字符串 str 在字符串s中出现的非重叠的次数
#### strings.Repeat(s, count int) string                 重复 count 次字符串 s 并返回一个新的字符串

#### strings.ToLower(s) string                           将字符串中的所有 字母 都转为对应的小写字符
#### strings.ToUpper(s) string                           将字符串中的所有 字母 都转为对应的大写字符

#### strings.TrimSpace(s string) string                  去除字符串两侧的空白字符
#### strings.Trim(s string, cutset string) string        去掉字符串两侧的 cutset 字符串中的任意字符
#### strings.TrimLeft(s string, cutset string) string    去掉字符串左侧的 cutset 字符串中的任意字符
#### strings.TrimRight(s string, cutset string) string   去掉字符串右侧的 cutset 字符串中的任意字符
#### strings.TrimPrefix(s string, cutset string) string  去掉字符串开头的 cutset 字符串
#### strings.TrimSuffix(s string, cutset string) string  去掉字符串结尾的 cutset 字符串

#### strings.Fields(s string) []string                   以 1个或多个空白字 符分隔字符串 s 并返回 slice
#### strings.Split(s string, sep string) []string        以 sep 分隔字符串 s 并返回 slice
#### strings.Join(a []string, sep string) string         将元素类型为 string的slice 使用 sep 拼接返回一个字符串

#### strings.Replace(s, old, new string, n int) string   替换 返回 s 的副本，并将副本中的 old 字符串替换为 new 字符串

#### strings.EqualFold(s, t) bool                        判断两个字符串是否相等，忽略大小写，同时它还会对特殊字符进行转换