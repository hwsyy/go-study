package main_example

import (
	"fmt"
	"strings"
)

func Example() {

	str := "判断字符串 str 是否以 prefix 开头"
	prefix := "判断"
	isStr := strings.HasPrefix(str, prefix)
	fmt.Println(isStr)

	str = "判断字符串 str 是否以 suffix 结尾"
	suffix := "结尾"
	isStr = strings.HasSuffix(str, suffix)
	fmt.Println(isStr)

	str = "判断字符串 str 是否包含 substr"
	substr := "包含"
	isStr = strings.Contains(str, substr)
	fmt.Println(isStr)

	//Index返回字符串substr在字符串str中的第一个索引(-1表示字符串str不包含字符串substr)，索引从0开始
	intStr := strings.Index(str, substr)
	fmt.Printf("'%s' 在字符串 '%s' 中的位置是:%d\n", substr, str, intStr)

	//Index返回字符串substr在字符串str中最后出现位置的索引(-1表示字符串str不包含字符串substr)，索引从0开始
	intStr = strings.LastIndex(str, substr)
	fmt.Printf("'%s' 在字符串 '%s' 中最后出现的位置是:%d\n", substr, str, intStr)

	str = "计算字符串 substr 在字符串 str 中出现的非重叠的次数"
	substr = "字符串"
	countStr := strings.Count(str, substr)
	fmt.Println(countStr)

	str = "重复的字符串|"
	str = strings.Repeat(str, 2)
	fmt.Printf("重复后: '%s'\n", str)

	str = " 去除字符串两侧的空白字符 		"
	str = strings.TrimSpace(str)
	fmt.Printf("去掉两边的空格后字符串为：'%s'\n", str)

	str = "去掉字符串两侧的 cutset 字符串中的任意字符去掉"
	cutset := "去掉"
	str = strings.Trim(str, cutset)
	fmt.Printf("去掉字符串两侧的任意字符：'%s'\n", str)

	str = "空白字 符分隔		字符串,sep"
	fmt.Printf("Fields : %q\n", strings.Fields(str))    // 以空格分隔字符串
	fmt.Printf("Split : %q\n", strings.Split(str, ",")) // 分隔字符串
	fmt.Printf("Join : %q\n", strings.Join([]string{"拼接", "返回一个", "字符串"}, ","))

	//替换字符串
	fmt.Println(strings.Replace("替换返字符串返", "返", "", -1))

	//判断两个字符串是否相等，忽略大小写，同时它还会对特殊字符进行转换
	//比如将“ϕ”转换为“Φ”、将“Ǆ”转换为“ǅ”等，然后再进行比较
	s1 := "Hello 世界! ϕ Ǆ"
	s2 := "hello 世界! Φ ǅ"
	isStr = strings.EqualFold(s1, s2)
	fmt.Printf("%v\n", isStr)
}
