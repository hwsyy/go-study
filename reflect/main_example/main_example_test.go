package main_example

import (
	"fmt"
	"testing"
)

/**
反射 reflection

反射可大大提高程序的灵活性，使得 interface{} 有更大的发挥余地
反射使用 TypeOf 和 ValueOf 函数从接口中获取目标对象信息
反射会将匿名字段作为独立字段（匿名字段本质）
想要利用反射修改对象状态，前提是 interface.data 是 settable，

即 pointer-interface
- 通过反射可以“动态”调用方法
*/

type User struct {
	Id   int
	Name string
	Age  int
}

func (u User) Hello(str string) int {
	fmt.Println("Hello world.", str)
	return len(str)
}

type Manager struct {
	Title string
	User
}

func TestStructInfo(t *testing.T) {

	u := User{1, "ok", 2}

	// 传入一个空接口，取出整个结构的信息
	StructInfo(u)

	fmt.Println()

}

func TestStructSetVal(t *testing.T) {

	u := User{1, "ok", 2}

	// 结构的反射操作-修改字段
	StructSetVal(&u, "Name", "laiki")

	fmt.Println(u)
	fmt.Println()
}

func TestStructSetFunc(t *testing.T) {

	u := User{}

	// 结构的反射操作-调用方法
	StructSetFunc(&u, "Hello", "laiki")

	fmt.Println()

}

func TestStructSetManager(t *testing.T) {

	m := Manager{
		Title:"manager",
		User:User{100, "lai", 12},
	}

	StructSetManager(m)

}

func TestSetInt64(t *testing.T) {

	var i int64 = 12

	// 修改一个 int64 类型的值
	SetInt64(&i, 1200)

	fmt.Println(i)
	fmt.Println()

}
