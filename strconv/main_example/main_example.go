package main_example

import(
	"fmt"
	"strconv"
)

func Example(){

	// 将整数转换为十进制字符串形式，即：FormatInt(i, 10) 的简写
	fmt.Println(strconv.Itoa(10))
	//fmt.Println(strconv.FormatInt(10,10))

	// 将字符串转换为十进制整数，即：ParseInt(s, 10, 0) 的简写
	intId,err := strconv.Atoi("10")
	//intId,err := strconv.ParseInt("10",10,0)
	if err !=nil {
		fmt.Println(err.Error())
	}
	fmt.Println(intId)


	// 将浮点数转换为字符串形式
	fmt.Println(strconv.FormatFloat(10.10,'f',3,32))

	// 将布尔值转换为字符串 true 或 false
	fmt.Println(strconv.FormatBool(true))
	// 将字符串转换为布尔值
	// 它接受真值：1, t, T, TRUE, true, True
	// 它接受假值：0, f, F, FALSE, false, False
	// 其它任何值都返回一个错误。
	fmt.Println(strconv.ParseBool("true"))
}
