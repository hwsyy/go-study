
## 数据类型转换

### 将整数转换为十进制字符串形式,即：FormatInt(i, 10) 的简写
#### strconv.Itoa(i int) string
### 将字符串转换为十进制整数，即：ParseInt(s, 10, 0) 的简写
#### strconv.Atoi(s string) (int, error)

### 将整数转换为字符串形式，base 表示转换进制(2 到 36)
#### strconv.FormatInt(i int64, base int) string
#### strconv.FormatUint(i uint64, base int) string

### 将字符串解析为整数, base 表示进位制(2 到 36),bitSize 表示结果的位宽(包括符号位),0 表示最大位宽
#### 如果 base 为 0，则根据字符串前缀判断，前缀 0x 表示 16 进制，前缀 0 表示 8 进制，否则是 10 进制
#### strconv.ParseInt(s string, base int, bitSize int) (i int64, err error)
#### strconv.ParseUint(s string, base int, bitSize int) (uint64, error)

### 将浮点数转换为字符串形式
#### f：要转换的浮点数
#### fmt：格式标记 b、e、E、f、g、G
#### prec：精度(数字部分的长度，不包括指数部分)
#### 格式标记：
##### 'b' (-ddddp±ddd，二进制指数)
##### 'e' (-d.dddde±dd，十进制指数)
##### 'E' (-d.ddddE±dd，十进制指数)
##### 'f' (-ddd.dddd，没有指数)
##### 'g' ('e':大指数，'f':其它情况)
##### 'G' ('E':大指数，'f':其它情况)
#### strconv.FormatFloat(f float64, fmt byte, prec, bitSize int) string

### 将字符串解析为浮点数,bigSize 取值有 32 和 64 两种，表示转换结果的精度
#### 如果有语法错误，则 err.Error = ErrSyntax
#### 如果结果超出范围，则返回 ±Inf,err.Error = ErrRange
#### strconv.ParseFloat(s string, bitSize int) (float64, error)


### 将布尔值转换为字符串 true 或 false
#### strconv.FormatBool(b bool) string
### 将字符串转换为布尔值,它接受真值：1、t、T、TRUE、true、True,它接受假值：0、f、F、FALSE、false、False
#### strconv.ParseBool(str string) (bool, error)