package block_example

import (
	"testing"
	"fmt"
)

func TestBlock(t *testing.T) {

	// 新建区块链
	bc := NewBlockChain()
	bc.AddBlock("第一块")
	bc.AddBlock("第二块")

	for k, v := range bc.Blocks {

		fmt.Println("================")
		fmt.Printf("第 %d 个区块信息:\n", k)
		fmt.Printf("PrevHash: %x\n", v.PrevHash)
		fmt.Printf("Data: %s\n", v.Data)
		fmt.Printf("Hash: %x\n", v.Hash)

	}

}
