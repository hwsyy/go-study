package http_example

import (
	"testing"
	"net/http"
	"fmt"
	"io/ioutil"
)

func TestRun(t *testing.T) {

	res, err := http.Get("http://127.0.0.1?user=laiki&age=20")
	if err != nil {
		fmt.Println("Get: ", err)
		return
	}
	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("ReadAll:", err)
		return
	}

	fmt.Println(string(data))

}
